import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { WeatherConditionsComponent } from './weather-conditions/weather-conditions.component';

const routes: Routes = [
  {
    path: 'forecast/:zipcode',
    loadChildren: () => import('./weather-forecast/weather-forecast.module').then(mod => mod.WeatherForecastModule)
  },
  {
    path: 'forecast/:zipcode/:countryCode',
    loadChildren: () => import('./weather-forecast/weather-forecast.module').then(mod => mod.WeatherForecastModule)
  },
  { path: '**', component: WeatherConditionsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
