import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SwUpdate } from '@angular/service-worker';

import { AppComponent } from './app.component';
import { APPLIATION_CONFIGURATION } from './app.config';
import { APP_CONFIG } from './app.module';
import { HeaderComponent } from './header/header.component';
import { SharedModule } from './shared/shared.module';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        SharedModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent
      ],
      providers: [
        { provide: APP_CONFIG, useValue: APPLIATION_CONFIGURATION },
        { provide: SwUpdate, useValue: {} }
      ]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
