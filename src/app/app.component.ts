import { Component, OnInit } from '@angular/core';

import { AppShellUpdateService } from './core/app-shell-update.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'wfm-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  installPromptEvent!: any;
  installEnabled = false;

  constructor(private appShellUpdateService: AppShellUpdateService) {
    window.addEventListener('beforeinstallprompt', event => {
      // Prevent Chrome <= 67 from automatically showing the prompt
      event.preventDefault();

      // Stash the event so it can be triggered later.
      this.installPromptEvent = event;

      // Update the install UI to notify the user app can be installed
      this.installEnabled = true;
    });
  }

  ngOnInit(): void {
    if (environment.production) {
      this.appShellUpdateService.subscribeForUpdates();
      this.appShellUpdateService.checkForUpdate();
    }
  }

  install() {
    this.installEnabled = false;

    // Show the modal add to home screen dialog
    this.installPromptEvent.prompt();

    // Wait for the user to respond to the prompt
    this.installPromptEvent.userChoice.then((choice: any) => {
      if (choice.outcome === 'accepted') {
        console.log('User accepted the A2HS prompt');
      } else {
        console.log('User dismissed the A2HS prompt');
      }

      // Clear the saved prompt since it can't be used again
      this.installPromptEvent = null;
    });
  }
}
