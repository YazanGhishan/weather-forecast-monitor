export interface AppConfig {
  googleApiKey: string,
  locationCodesStorageKey: string,
  weatherIntervalMilliseconds: number,
  weatherApiBaseUrl: string,
  weatherApiAppId: string,
  weatherConditionsUrl: string,
  weatherForecastUrl: string
};
