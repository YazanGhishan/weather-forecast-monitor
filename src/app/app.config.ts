import { AppConfig } from './app.config.model';

export const APPLIATION_CONFIGURATION: AppConfig = {
  googleApiKey: '',
  locationCodesStorageKey: 'locationCodes',
  weatherIntervalMilliseconds: 30000,
  weatherApiBaseUrl: 'https://api.openweathermap.org',
  weatherApiAppId: '5a4b2d457ecbef9eb2a71e480b947604',
  weatherConditionsUrl: '/data/2.5/weather',
  weatherForecastUrl: '/data/2.5/forecast/daily'
};
