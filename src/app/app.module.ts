import { InjectionToken, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { WeatherConditionsModule } from './weather-conditions/weather-conditions.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { AppConfig } from './app.config.model';
import { APPLIATION_CONFIGURATION } from './app.config';
import { httpInterceptorProviders } from './http-interceptors';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    WeatherConditionsModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
      // Register the ServiceWorker as soon as the app is stable
      // or after 30 seconds (whichever comes first).
      registrationStrategy: 'registerWhenStable:30000'
    })
  ],
  providers: [
    { provide: APP_CONFIG, useValue: APPLIATION_CONFIGURATION },
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
