import { Injectable } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';

import { SnackbarService } from './snackbar.service';

@Injectable({
  providedIn: 'root'
})
export class AppShellUpdateService {
  constructor(
    private swUpdate: SwUpdate,
    private snackbarService: SnackbarService
  ) { }

  subscribeForUpdates(): void {
    this.swUpdate.available.subscribe((event: any) => {
      console.log(
        '[App Shell Update] Update available: current version is',
        event.current,
        'available version is',
        event.available
      );

      let versionMessage = event.available.appData ? event.available.appData['versionMessage'] : '';
      let snackBarRef = this.snackbarService.openSnackBar(
        versionMessage || 'Newer version of the app is available.',
        'Refresh the page'
      );

      snackBarRef.onAction().subscribe(() => {
        this.activateUpdate();
      });
    });

    this.swUpdate.activated.subscribe(event => {
      console.log(
        '[App Shell Update] Update activated: old version was',
        event.previous,
        'new version is',
        event.current
      );
    });
  }

  checkForUpdate(): void {
    this.swUpdate
      .checkForUpdate()
      .then(() => { })
      .catch(err => console.error(err));
  }

  activateUpdate(): void {
    this.swUpdate
      .activateUpdate()
      .then(() => window.location.reload())
      .catch(err => console.error(err));
  }
}
