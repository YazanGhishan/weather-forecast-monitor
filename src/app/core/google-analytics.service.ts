import { Inject, Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import { APP_CONFIG } from '../app.module';
import { AppConfig } from '../app.config.model';

declare let ga: Function;

@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {
  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private router: Router) { }

  initializeGoogleAnalytics(): void {
    if (this.config.googleApiKey) {
      ga('create', this.config.googleApiKey, 'auto');

      this.router.events.subscribe((event: any) => {
        if (navigator.onLine && event instanceof NavigationEnd) {
          ga('set', 'page', event.urlAfterRedirects);
          ga('send', 'pageview')
        }
      });
    }
  }

  emitEvent(
    eventCategory: string,
    eventAction: string,
    eventLabel?: string,
    eventValue?: number): void {
    if (this.config.googleApiKey && navigator.onLine) {
      ga('send', 'event', {
        eventCategory: eventCategory,
        eventLabel: eventLabel || null,
        eventAction: eventAction,
        eventValue: eventValue ? Math.round(eventValue) : null
      });
    }
  }
}
