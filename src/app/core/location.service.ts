import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {
  getCurrentPosition(options?: any): Observable<any> {
    return new Observable((observer: any) => {
      if (window.navigator && window.navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition(
          (position: any) => {
            observer.next(position);
            observer.complete();
          },
          (error: any) => observer.error(error),
          options ?? { timeout: 10000 }
        );
      } else {
        observer.error('Location services unsupported');
      }
    });
  }
}
