import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarRef, TextOnlySnackBar } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class SnackbarService {
  constructor(private snackBar: MatSnackBar) { }

  openSnackBar(message: string, action = 'close', duration = 3000): MatSnackBarRef<TextOnlySnackBar> {
    return this.snackBar.open(message, action, { duration: duration });
  }
}
