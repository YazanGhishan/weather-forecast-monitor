import { Injectable } from '@angular/core';

import { LocationCode } from '../shared/location-code';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  getItem(key: string): string | null {
    const rawItems = localStorage.getItem(key);
    return rawItems;
  }

  setItem(key: string, data: string): void {
    localStorage.setItem(key, JSON.stringify(data));
  }

  getArray(key: string): Object[] {
    const rawItems = localStorage.getItem(key);
    return rawItems ? JSON.parse(rawItems) : [];
  }

  setArray(key: string, data: Object[]): void {
    localStorage.setItem(key, JSON.stringify(data));
  }
}
