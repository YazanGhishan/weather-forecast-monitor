import { fakeAsync, flush, tick } from '@angular/core/testing';

import { takeWhile } from 'rxjs/operators';

import { TimerService } from './timer.service';

describe('TimerService', () => {
  let service: TimerService;
  const config: any = { weatherIntervalMilliseconds: 1000 };

  beforeEach(() => {
    service = new TimerService(config);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should tick periodically', fakeAsync(() => {
    let num: number = 0;
    let alive = true;
    let subscription = service.weatherTimer$
      .pipe(takeWhile(() => alive))
      .subscribe((data: number) => num += data);

    tick(10500);
    alive = false;
    tick(1000); // To complete the timer
    flush();
    subscription.unsubscribe();

    expect(num).toBe(55);
  }));
});
