import { Inject, Injectable } from '@angular/core';

import { timer } from 'rxjs';

import { APP_CONFIG } from '../app.module';
import { AppConfig } from '../app.config.model';

@Injectable({
  providedIn: 'root'
})
export class TimerService {
  weatherTimer$ = timer(0, this.config.weatherIntervalMilliseconds);

  constructor(@Inject(APP_CONFIG) private config: AppConfig) { }
}
