import { Observable } from 'rxjs';
import { WeatherService } from './weather.service';

describe('WeatherService', () => {
  let service: WeatherService;
  let config: any;
  let http: any;
  let timerService: any = { weatherTimer$: new Observable() };
  let locationService: any;

  beforeEach(() => {
    service = new WeatherService(config, http, timerService, locationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
