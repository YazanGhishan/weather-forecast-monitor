import { Inject, Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Subject, Observable, of } from 'rxjs';
import { switchMap, catchError } from 'rxjs/operators'

import { APP_CONFIG } from '../app.module';
import { AppConfig } from '../app.config.model';
import { TimerService } from './timer.service';
import { LocationService } from './location.service';
import { LocationCode } from '../shared/location-code';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private localWeatherConditionsSubject = new Subject<any>();
  localWeatherConditions$ = this.localWeatherConditionsSubject.asObservable();

  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient,
    private timerService: TimerService,
    private locationService: LocationService) {
    this.timerService.weatherTimer$
      .subscribe(() => this.getLocalWeatherConditions());
  }

  private getLocalWeatherConditions(): void {
    this.locationService.getCurrentPosition()
      .pipe(
        switchMap((position: any) => {
          const params = new HttpParams()
            .set('lat', position.coords.latitude)
            .set('lon', position.coords.longitude)
            .set('units', 'imperial');

          return this.http.get<any>(this.config.weatherConditionsUrl, { params });
        }),
        catchError(() => of(null))
      ).subscribe((conditions: any) =>
        this.localWeatherConditionsSubject.next(conditions));
  }

  getWeatherForecast(locationCode: LocationCode): Observable<any> {
    const zip = locationCode.zipcode +
      (locationCode.countryCode ? `,${locationCode.countryCode}` : '');

    const params = new HttpParams()
      .set('zip', zip)
      .set('units', 'imperial')
      .set('cnt', 5);

    return this.http.get<any>(this.config.weatherForecastUrl, { params });
  }
}
