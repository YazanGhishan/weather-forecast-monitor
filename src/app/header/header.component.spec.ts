import { Observable } from 'rxjs';
import { HeaderComponent } from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let weatherService: any = { localWeatherConditions$: new Observable() };

  beforeEach(() => {
    component = new HeaderComponent(weatherService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
