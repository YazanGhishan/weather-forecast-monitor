import { Component, EventEmitter, Input, Output } from '@angular/core';
import { WeatherService } from '../core/weather.service';

@Component({
  selector: 'wfm-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() installEnabled = false;
  @Output() install = new EventEmitter();

  conditions: any;

  constructor(private weatherService: WeatherService) {
    this.weatherService.localWeatherConditions$
      .subscribe((conditions: any) => this.conditions = conditions);
  }

  installApp() {
    this.install.emit();
  }
}
