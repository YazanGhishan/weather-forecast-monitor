import { TestBed } from '@angular/core/testing';

import { APPLIATION_CONFIGURATION } from '../app.config';
import { APP_CONFIG } from '../app.module';

import { AuthInterceptor } from './auth.interceptor';

describe('AuthInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: APP_CONFIG, useValue: APPLIATION_CONFIGURATION },
      AuthInterceptor
    ]
  }));

  it('should be created', () => {
    const interceptor: AuthInterceptor = TestBed.inject(AuthInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
