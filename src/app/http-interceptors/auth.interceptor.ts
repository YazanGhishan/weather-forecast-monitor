import { Inject, Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';

import { Observable } from 'rxjs';

import { APP_CONFIG } from '../app.module';
import { AppConfig } from '../app.config.model';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(@Inject(APP_CONFIG) private config: AppConfig) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.startsWith(this.config.weatherApiBaseUrl)) {
      const weatherApiAppId = this.config.weatherApiAppId;

      const authReq = request.clone({
        params: request.params.append('appid', weatherApiAppId)
      });

      return next.handle(authReq);
    } else {
      return next.handle(request);
    }
  }
}
