import { TestBed } from '@angular/core/testing';

import { APPLIATION_CONFIGURATION } from '../app.config';
import { APP_CONFIG } from '../app.module';

import { BaseUrlInterceptor } from './base-url.interceptor';

describe('BaseUrlInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      { provide: APP_CONFIG, useValue: APPLIATION_CONFIGURATION },
      BaseUrlInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: BaseUrlInterceptor = TestBed.inject(BaseUrlInterceptor);
    expect(interceptor).toBeTruthy();
  });
});
