import { Inject, Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';

import { Observable } from 'rxjs';

import { APP_CONFIG } from '../app.module';
import { AppConfig } from '../app.config.model';

@Injectable()
export class BaseUrlInterceptor implements HttpInterceptor {
  constructor(@Inject(APP_CONFIG) private config: AppConfig) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url.startsWith('/data/')) {
      const weatherApiBaseUrl = this.config.weatherApiBaseUrl;

      const baseReq = request.clone({
        url: weatherApiBaseUrl + request.url
      });

      return next.handle(baseReq);
    } else {
      return next.handle(request);
    }
  }
}
