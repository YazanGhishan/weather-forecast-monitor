import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AutocompleteComponent } from './autocomplete.component';

describe('AutocompleteComponent', () => {
  let component: AutocompleteComponent;
  let fixture: ComponentFixture<AutocompleteComponent>;

  const options = [
    { key: 'A', description: 'Abc' },
    { key: 'B', description: 'Def' },
    { key: 'C', description: 'Fgh' },
    { key: 'D', description: 'Ghi' },
    { key: 'E', description: 'Hij' },
    { key: 'F', description: 'Wxyz' },
    { key: 'G', description: 'Xyz' }
  ];

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [AutocompleteComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AutocompleteComponent);
    component = fixture.componentInstance;
    component.options = options;
    component.display = 'description';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show all the options', () => {
    expect(component.filteredOptions).toBe(options);
  });

  it('should filter options with a single letter', () => {
    const expectedResult = [{ key: 'A', description: 'Abc' }];
    component.searchValue = 'b';

    component.filterOptions();

    expect(component.filteredOptions).toEqual(expectedResult);
  });

  it('should filter options with a single letter regardless of case', () => {
    const expectedResult = [
      { key: 'C', description: 'Fgh' },
      { key: 'D', description: 'Ghi' }
    ];
    component.searchValue = 'g';

    component.filterOptions();

    expect(component.filteredOptions).toEqual(expectedResult);
  });

  it('should filter options with multiple letters', () => {
    const expectedResult = [
      { key: 'F', description: 'Wxyz' },
      { key: 'G', description: 'Xyz' }
    ];
    component.searchValue = 'yz';

    component.filterOptions();

    expect(component.filteredOptions).toEqual(expectedResult);
  });

  it('should filter options with multiple letters regardless of case', () => {
    const expectedResult = [
      { key: 'C', description: 'Fgh' },
      { key: 'D', description: 'Ghi' }
    ];
    component.searchValue = 'gH';

    component.filterOptions();

    expect(component.filteredOptions).toEqual(expectedResult);
  });

  it('should ignore leading whitespaces when filtering', () => {
    const expectedResult = [
      { key: 'D', description: 'Ghi' },
      { key: 'E', description: 'Hij' },
    ];
    component.searchValue = '          i';

    component.filterOptions();

    expect(component.filteredOptions).toEqual(expectedResult);
  });

  it('should ignore trailing whitespaces when filtering', () => {
    const expectedResult = [
      { key: 'F', description: 'Wxyz' },
      { key: 'G', description: 'Xyz' }
    ];
    component.searchValue = 'XYZ      ';

    component.filterOptions();

    expect(component.filteredOptions).toEqual(expectedResult);
  });

  it('should ignore leading and trailing whitespaces when filtering', () => {
    const expectedResult = [
      { key: 'C', description: 'Fgh' },
      { key: 'D', description: 'Ghi' }
    ];
    component.searchValue = '    gH      ';

    component.filterOptions();

    expect(component.filteredOptions).toEqual(expectedResult);
  });
});
