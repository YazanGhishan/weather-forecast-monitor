import { Component, ElementRef, EventEmitter, HostListener, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'wfm-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit, OnChanges {
  @Input() disabled = false;
  @Input() placeholder = 'Select option';
  @Input() options!: any[];
  @Input() display?: string;
  @Input() noOptionsMessage = 'No options available';
  @Input() selection!: any;
  @Output() selectionChange: EventEmitter<any> = new EventEmitter();

  @HostListener('document:click', ['$event'])
  clickout(event: Event) {
    // This closes the autocomplete if the user clicks outside of it.
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.isOpen = false;
    }
  }

  searchValue = '';
  isOpen = false;
  filteredOptions!: any[];

  constructor(private elementRef: ElementRef) { }

  ngOnInit(): void {
    this.searchValue = this.getSearchValue()
    this.filteredOptions = this.options;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.selection?.currentValue) {
      this.searchValue = this.getSearchValue();
      this.filterOptions();
    }
  }

  getSearchValue(): string {
    return (this.display && this.selection) ? this.selection[this.display] : this.selection;
  }

  filterOptions(): void {
    this.filteredOptions = this.options.filter((option: any) =>
      (this.display ? option[this.display] : option)
        .toLowerCase()
        .indexOf(this.searchValue?.toLowerCase().trim()) > -1
    );
  }

  selectOption(option: any): void {
    this.selection = option;
    this.selectionChange.emit(this.selection);
    this.isOpen = false;

    //Filter the options for the next time the dropdown is opened;
    this.filterOptions();
  }
}
