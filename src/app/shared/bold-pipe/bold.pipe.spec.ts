import { BoldPipe } from './bold.pipe';

describe('BoldPipe', () => {
  let pipe: BoldPipe;

  beforeEach(() => {
    pipe = new BoldPipe();
  });

  it('should create an instance', () => {
    expect(pipe).toBeTruthy();
  });

  it('should make a whole string bold with no parameter', () => {
    const value = 'United States';
    const expectedResult = '<b>United States</b>';

    const result = pipe.transform(value);

    expect(result).toBe(expectedResult);
  });

  it('should not make anything bold with an empty string parameter', () => {
    const value = 'United States';
    const boldText = '';
    const expectedResult = 'United States';

    const result = pipe.transform(value, boldText);

    expect(result).toBe(expectedResult);
  });

  it('should make a same case letter bold', () => {
    const value = 'United States';
    const boldText = 'a';
    const expectedResult = 'United St<b>a</b>tes';

    const result = pipe.transform(value, boldText);

    expect(result).toBe(expectedResult);
  });

  it('should make multiple same case letters bold', () => {
    const value = 'United States';
    const boldText = 'e'
    const expectedResult = 'Unit<b>e</b>d Stat<b>e</b>s';

    const result = pipe.transform(value, boldText);

    expect(result).toBe(expectedResult);
  });

  it('should make a same case sentence bold', () => {
    const value = 'abcdefghijklmnopqrstuvwxyz';
    const boldText = 'fghijklmnopqrstu'
    const expectedResult = 'abcde<b>fghijklmnopqrstu</b>vwxyz';

    const result = pipe.transform(value, boldText);

    expect(result).toBe(expectedResult);
  });

  it('should make multiple same case sentences bold', () => {
    const value = 'abcde abcde abcde';
    const boldText = 'cde'
    const expectedResult = 'ab<b>cde</b> ab<b>cde</b> ab<b>cde</b>';

    const result = pipe.transform(value, boldText);

    expect(result).toBe(expectedResult);
  });

  it('should ignore single letter case and preserve original text', () => {
    const value = 'Abc';
    const boldText = 'a'
    const expectedResult = '<b>A</b>bc';

    const result = pipe.transform(value, boldText);

    expect(result).toBe(expectedResult);
  });

  it('should ignore sentence case and preserve original text', () => {
    const value = 'The boy went to the school.';
    const boldText = 'Th'
    const expectedResult = '<b>Th</b>e boy went to <b>th</b>e school.';

    const result = pipe.transform(value, boldText);

    expect(result).toBe(expectedResult);
  });
});
