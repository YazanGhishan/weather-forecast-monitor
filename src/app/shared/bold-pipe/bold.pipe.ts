import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'bold'
})
export class BoldPipe implements PipeTransform {
  transform(value: string, boldText?: string | undefined | null): string {
    if (boldText) {
      const pattern = `(${boldText.trim()})`; // Brackets needed to capture original value in $1.
      const regexp = new RegExp(pattern, 'gi'); // Match all and ignore case
      const replaceValue = `<b>$1</b>`; // Preserve original case when adding bold.

      return value.replace(regexp, replaceValue);
    } else {
      return boldText === '' ? value : `<b>${value}</b>`;
    }
  }
}
