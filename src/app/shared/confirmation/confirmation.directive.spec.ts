import { MatDialog } from '@angular/material/dialog';

import { ConfirmationDirective } from './confirmation.directive';

describe('ConfirmationDirective', () => {
  let dialog: MatDialog;

  it('should create an instance', () => {
    const directive = new ConfirmationDirective(dialog);
    expect(directive).toBeTruthy();
  });
});
