import { Directive, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { ConfirmationComponent } from './confirmation.component';

@Directive({
  selector: '[wfmConfirmation]'
})
export class ConfirmationDirective {
  @Input() wfmConfirmation!: string;
  @Output() confirm = new EventEmitter();

  @HostListener('click') onClick() {
    this.openConfirmationDialog();
  }

  constructor(private dialog: MatDialog) { }

  openConfirmationDialog() {
    const config = { data: this.wfmConfirmation };
    const dialogRef = this.dialog.open(ConfirmationComponent, config);

    dialogRef.afterClosed().subscribe((result: boolean) => this.confirm.emit(result));
  }
}
