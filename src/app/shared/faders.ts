import { animation, style, animate } from "@angular/animations";

export const fadeIn = animation(
  [
    style({ opacity: 0 }),
    animate('{{timing}}ms {{delay}}ms ease-in', style({ opacity: 1 }))
  ],
  {
    params: { timing: 300, delay: 0 }
  }
);

export const fadeOut = animation(
  [
    animate('{{timing}}ms {{delay}}ms ease-out', style({ opacity: 0 }))
  ],
  {
    params: { timing: 300, delay: 0 }
  }
);
