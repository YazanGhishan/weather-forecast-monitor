export interface LocationCode {
  zipcode: string;
  countryCode: string | undefined | null;
}
