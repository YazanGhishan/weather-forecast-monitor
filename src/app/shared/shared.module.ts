import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FlexLayoutModule } from '@angular/flex-layout';

import { MaterialModule } from './material/material.module';
import { StateButtonComponent } from './state-button/state-button.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { WeatherImageComponent } from './weather-image/weather-image.component';
import { ConfirmationDirective } from './confirmation/confirmation.directive';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { BoldPipe } from './bold-pipe/bold.pipe';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    MaterialModule
  ],
  exports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule,
    MaterialModule,
    StateButtonComponent,
    AutocompleteComponent,
    WeatherImageComponent,
    ConfirmationDirective
  ],
  declarations: [
    StateButtonComponent,
    AutocompleteComponent,
    WeatherImageComponent,
    ConfirmationDirective,
    ConfirmationComponent,
    BoldPipe
  ]
})
export class SharedModule { }
