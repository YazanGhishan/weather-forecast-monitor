export enum ButtonState {
  READY = 'ready',
  WORKING = 'working',
  DONE = 'done'
};
