import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, SimpleChanges, TemplateRef } from '@angular/core';

import { Subject } from 'rxjs';

import { ButtonState } from './button-state.enum';

@Component({
  selector: 'wfm-state-button',
  templateUrl: './state-button.component.html',
  styleUrls: ['./state-button.component.scss']
})
export class StateButtonComponent implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input() disabled = false;
  @Input() contentsTemplate!: TemplateRef<any>;
  @Input() disabledStyle!: any;
  @Input() readyStyle!: any;
  @Input() workingStyle!: any;
  @Input() doneStyle!: any;
  @Input() state!: ButtonState;
  @Output() stateChange = new EventEmitter<ButtonState>();

  ButtonState = ButtonState; // To use the enum in the template
  buttonStyle: any;

  private stateSubject!: Subject<ButtonState>;

  private defaultStyles = {
    disabled: { 'background-color': '#0000001F', color: '#00000042' },
    ready: { 'background-color': '#3F51B5', color: 'white' }, // mat-indigo 500
    working: { 'background-color': '#FFEB3B' }, // mat-yellow 500
    done: { 'background-color': '#4CAF50' } // mat-green 500
  }

  ngOnInit(): void {
    // Setting default states
    this.disabledStyle = this.disabledStyle || this.defaultStyles.disabled;
    this.readyStyle = this.readyStyle || this.defaultStyles.ready;
    this.workingStyle = this.workingStyle || this.defaultStyles.working;
    this.doneStyle = this.doneStyle || this.defaultStyles.done;
  }

  ngAfterViewInit(): void {
    setTimeout(() => this.setState(ButtonState.READY), 0);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.disabled) {
      this.setButtonStyle();
    }

    if (changes.state && changes.state.currentValue === ButtonState.DONE) {
      this.stateSubject.complete();
    }

    if (changes.disabledStyle) {
      this.disabledStyle = this.disabledStyle || this.defaultStyles.disabled;
    }

    if (changes.readyStyle) {
      this.readyStyle = this.readyStyle || this.defaultStyles.ready;
    }

    if (changes.workingStyle) {
      this.workingStyle = this.workingStyle || this.defaultStyles.working;
    }

    if (changes.doneStyle) {
      this.doneStyle = this.doneStyle || this.defaultStyles.done;
    }
  }

  ngOnDestroy(): void {
    if (this.stateSubject) {
      this.stateSubject.unsubscribe();
    };
  }

  onClick(): void {
    if (this.state === ButtonState.READY) {
      this.stateSubject = new Subject<ButtonState>();

      this.stateSubject.subscribe({
        next: (state: ButtonState) => {
          this.state = state;
          this.setButtonStyle();

          if (this.state === ButtonState.WORKING) {
            this.stateChange.emit(ButtonState.WORKING);
          }
        },
        error: (err: Error) => {
          this.setState(ButtonState.READY);
        },
        complete: () => {
          this.setButtonStyle();
        }
      });

      this.stateSubject.next(ButtonState.WORKING);
    }

    if (this.state === ButtonState.DONE) {
      this.setState(ButtonState.READY);
    }
  }

  private setState(state: ButtonState): void {
    this.state = state;
    this.stateChange.emit(this.state);
    this.setButtonStyle();
  }

  private setButtonStyle(): void {
    switch (this.state) {
      case ButtonState.READY:
        if (this.disabled) {
          this.buttonStyle = this.disabledStyle;
        } else {
          this.buttonStyle = this.readyStyle;
        }
        break;
      case ButtonState.WORKING:
        this.buttonStyle = this.workingStyle;
        break;
      case ButtonState.DONE:
        this.buttonStyle = this.doneStyle;
        break;
    }
  }
}
