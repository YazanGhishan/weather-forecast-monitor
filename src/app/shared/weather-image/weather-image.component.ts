import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'wfm-weather-image',
  templateUrl: './weather-image.component.html',
  styleUrls: ['./weather-image.component.scss']
})
export class WeatherImageComponent implements OnInit {
  @Input() weatherMain!: string;

  imageClass: any;

  ngOnInit(): void {
    this.imageClass = {
      sun: this.weatherMain === 'Clear',
      clouds: ['Clouds', 'Mist', 'Smoke', 'Haze', 'Dust', 'Fog', 'Sand', 'Ash', 'Squall', 'Tornado'].includes(this.weatherMain),
      rain: ['Rain', 'Thunderstorm', 'Drizzle'].includes(this.weatherMain),
      snow: this.weatherMain === 'Snow'
    }
  }
}
