import { WeatherConditionsComponent } from './weather-conditions.component';

describe('WeatherConditionsComponent', () => {
  let component: WeatherConditionsComponent;
  let config: any = { locationCodesStorageKey: 'testStorageKey' };
  let storageService: any = { setArray: () => { } };
  let googleAnalyticsService: any = { emitEvent: () => { } };
  let snackbarService: any = { openSnackBar: () => { } };

  beforeEach(() => {
    component = new WeatherConditionsComponent(
      config,
      storageService,
      googleAnalyticsService,
      snackbarService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should identify a unique zipcode', () => {
    component.locationCodes = [{ zipcode: '12345', countryCode: 'AA' }];
    const isUnique = component['isUniqueZipcode']('67890');
    expect(isUnique).toBe(true);
  });

  it('should identify a non-unique zipcode', () => {
    component.locationCodes = [{ zipcode: '12345', countryCode: 'AA' }];
    const isUnique = component['isUniqueZipcode']('12345');
    expect(isUnique).toBe(false);
  });

  it('should add a zipcode', () => {
    component.locationCodes = [];
    component.zipcode = '12345';
    component.addZipcode();
    expect(component.locationCodes).toEqual([{ zipcode: '12345', countryCode: undefined }]);
  });

  it('should add a zipcode and country code', () => {
    component.locationCodes = [];
    component.zipcode = '12345';
    component.selectedCountry = { code: 'AA', name: 'myCountry' }
    component.addZipcode();
    expect(component.locationCodes).toEqual([{ zipcode: '12345', countryCode: 'AA' }]);
  });

  it('should add a unique zipcode', () => {
    component.locationCodes = [{ zipcode: '12345', countryCode: 'AA' }];
    component.zipcode = '67890';
    component.addZipcode();
    expect(component.locationCodes).toEqual([{ zipcode: '67890', countryCode: undefined }, { zipcode: '12345', countryCode: 'AA' }]);
  });

  it('should add multiple unique zipcodes', () => {
    component.locationCodes = [{ zipcode: '12345', countryCode: '' }];
    component.zipcode = '23456';
    component.addZipcode();
    component.zipcode = '34567';
    component.selectedCountry = { code: 'AA', name: 'Country A' };
    component.addZipcode();
    component.zipcode = '45678';
    component.selectedCountry = { code: 'BB', name: 'Country B' };
    component.addZipcode();
    component.zipcode = '56789';
    component.selectedCountry = { code: 'CC', name: 'Country C' };
    component.addZipcode();
    component.zipcode = '67890';
    component.selectedCountry = { code: 'DD', name: 'Country D' };
    component.addZipcode();
    component.zipcode = '78901';
    component.selectedCountry = { code: 'EE', name: 'Country E' };
    component.addZipcode();

    const expectedResult = [
      { zipcode: '78901', countryCode: 'EE' },
      { zipcode: '67890', countryCode: 'DD' },
      { zipcode: '56789', countryCode: 'CC' },
      { zipcode: '45678', countryCode: 'BB' },
      { zipcode: '34567', countryCode: 'AA' },
      { zipcode: '23456', countryCode: undefined },
      { zipcode: '12345', countryCode: '' }
    ];

    expect(component.locationCodes).toEqual(expectedResult);
  });

  it('should not add a non-unique zipcode', () => {
    component.locationCodes = [{ zipcode: '12345', countryCode: '' }];
    component.zipcode = '12345';
    component.addZipcode();
    expect(component.locationCodes).toEqual([{ zipcode: '12345', countryCode: '' }]);
  });

  it('should not add multiple non-unique zipcodes', () => {
    component.locationCodes = [{ zipcode: '12345', countryCode: 'AA' }, { zipcode: '45678', countryCode: 'BB' }];
    component.zipcode = '12345';
    component.addZipcode();
    component.zipcode = '12345';
    component.addZipcode();
    component.zipcode = '12345';
    component.addZipcode();
    component.zipcode = '45678';
    component.addZipcode();
    component.zipcode = '45678';
    component.addZipcode();
    component.zipcode = '45678';
    component.addZipcode();
    expect(component.locationCodes).toEqual([{ zipcode: '12345', countryCode: 'AA' }, { zipcode: '45678', countryCode: 'BB' }]);
  });

  it('should only add unique zipcode', () => {
    component.locationCodes = [{ zipcode: '12345', countryCode: 'AA' }, { zipcode: '45678', countryCode: 'BB' }];
    component.zipcode = '12345';
    component.addZipcode();
    component.zipcode = '23456';
    component.addZipcode();
    component.zipcode = '34567';
    component.addZipcode();
    component.zipcode = '12345';
    component.addZipcode();
    component.zipcode = '23456';
    component.addZipcode();
    component.zipcode = '34567';
    component.addZipcode();
    component.zipcode = '45678';
    component.addZipcode();

    const expectedResult = [
      { zipcode: '34567', countryCode: undefined },
      { zipcode: '23456', countryCode: undefined },
      { zipcode: '12345', countryCode: 'AA' },
      { zipcode: '45678', countryCode: 'BB' }
    ];

    expect(component.locationCodes).toEqual(expectedResult);
  });

  it('should delete a zipcode', () => {
    component.locationCodes = [];
    component.zipcode = '12345';
    component.addZipcode();
    component.zipcode = '67890';
    component.addZipcode();
    component.deleteLocationCode({ zipcode: '12345', countryCode: '' });

    expect(component.locationCodes).toEqual([{ zipcode: '67890', countryCode: undefined }]);
  });

  it('should clear all zipcodes', () => {
    component.locationCodes = [];
    component.zipcode = '12345';
    component.addZipcode();
    component.zipcode = '23456';
    component.addZipcode();
    component.zipcode = '34567';
    component.addZipcode();
    component.zipcode = '45678';
    component.addZipcode();
    component.zipcode = '56789';
    component.addZipcode();
    component.zipcode = '67890';
    component.addZipcode();
    component.zipcode = '78901';
    component.addZipcode();
    component.clearAllLocations(true);

    expect(component.locationCodes).toEqual([]);
  });
});
