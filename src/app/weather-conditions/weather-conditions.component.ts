import { Component, Inject, OnInit } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { FormControl, Validators } from '@angular/forms';

import { APP_CONFIG } from '../app.module';
import { AppConfig } from '../app.config.model';
import { StorageService } from '../core/storage.service';
import { GoogleAnalyticsService } from '../core/google-analytics.service';
import { SnackbarService } from '../core/snackbar.service';
import { fadeIn } from 'src/app/shared/faders';
import { LocationCode } from '../shared/location-code';
import { ButtonState } from '../shared/state-button/button-state.enum';
import { Countries, Country } from './country';

@Component({
  selector: 'wfm-weather-conditions',
  templateUrl: './weather-conditions.component.html',
  styleUrls: ['./weather-conditions.component.scss'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        useAnimation(fadeIn)
      ])
    ])
  ]
})
export class WeatherConditionsComponent implements OnInit {
  zipcode!: string;
  locationCodes!: LocationCode[];
  confirmDelete!: string;
  saveButtonState!: ButtonState;
  ButtonState = ButtonState; // To use the enum in the template
  countries = [{ code: '', name: '' }, ...Countries];
  selectedCountry!: Country;

  zipcodeFormControl = new FormControl('', [
    Validators.pattern('^[0-9]{4,5}(?:-[0-9]{4})?$')
  ]);

  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private storageService: StorageService,
    private googleAnalyticsService: GoogleAnalyticsService,
    private snackbarService: SnackbarService) { }

  ngOnInit(): void {
    this.confirmDelete = 'Are you sure you want to clear all locations?';
    this.locationCodes = <LocationCode[]>this.storageService.getArray(this.config.locationCodesStorageKey);
  }

  onButtonStateChange(state: ButtonState): void {
    this.saveButtonState = state;

    if (this.saveButtonState === ButtonState.WORKING) {
      this.addZipcode();
      this.zipcode = '';
      this.selectedCountry = { code: '', name: '' };
    }
  }

  onZipcodeKeyup(value: string): void {
    this.zipcode = value;

    if (this.saveButtonState === ButtonState.DONE) {
      this.saveButtonState = ButtonState.READY; // Reset the state button
    }
  }

  addZipcode(): void {
    if (!this.isUniqueZipcode(this.zipcode)) {
      this.snackbarService.openSnackBar(`Zipcode ${this.zipcode} already exists`);
      this.completeStateButton(); // Complete the button to be able to reset it without an API response
    } else {
      this.locationCodes.unshift({ zipcode: this.zipcode, countryCode: this.selectedCountry?.code });
      this.storageService.setArray(this.config.locationCodesStorageKey, this.locationCodes);
      this.googleAnalyticsService.emitEvent('location', 'added', this.zipcode, new Date().getTime());
    }
  }

  completeStateButton(): void {
    if (this.saveButtonState === ButtonState.WORKING) {
      this.saveButtonState = ButtonState.DONE;
    }
  }

  clearAllLocations(event: boolean): void {
    if (event) {
      this.locationCodes = [];
      this.storageService.setArray(this.config.locationCodesStorageKey, this.locationCodes);
    }
  }

  deleteLocationCode(locationCode: LocationCode): void {
    this.locationCodes = this.locationCodes.filter((item: LocationCode) => item.zipcode !== locationCode.zipcode);
    this.storageService.setArray(this.config.locationCodesStorageKey, this.locationCodes);
    this.googleAnalyticsService.emitEvent('location', 'deleted', locationCode.zipcode, new Date().getTime());
  }

  private isUniqueZipcode(zipcode: string): boolean {
    return !this.locationCodes.find((locationCode: LocationCode) => locationCode.zipcode === zipcode);
  }
}
