import { NgModule } from '@angular/core';

import { SharedModule } from '../shared/shared.module';
import { WeatherConditionsComponent } from './weather-conditions.component';
import { WeatherItemComponent } from './weather-item/weather-item.component';

@NgModule({
  declarations: [
    WeatherConditionsComponent,
    WeatherItemComponent
  ],
  imports: [
    SharedModule
  ]
})
export class WeatherConditionsModule { }
