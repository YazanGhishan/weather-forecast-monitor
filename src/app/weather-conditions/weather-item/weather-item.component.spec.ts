import { Observable } from 'rxjs';

import { WeatherItemComponent } from './weather-item.component';

describe('WeatherItemComponent', () => {
  let component: WeatherItemComponent;
  let weatherItemService: any = { weatherConditions$: new Observable() };
  let svc: any;

  beforeEach(() => {
    component = new WeatherItemComponent(weatherItemService, svc);
  });

  it('should create', () => {
    component.locationCode = { zipcode: '12345', countryCode: 'AA' };
    expect(component).toBeTruthy();
  });
});
