import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';

import { takeWhile } from 'rxjs/operators';

import { WeatherItemService } from './weather-item.service';
import { SnackbarService } from 'src/app/core/snackbar.service';
import { fadeIn } from 'src/app/shared/faders';
import { LocationCode } from 'src/app/shared/location-code';

@Component({
  selector: 'wfm-weather-item',
  templateUrl: './weather-item.component.html',
  styleUrls: ['./weather-item.component.scss'],
  providers: [WeatherItemService],
  animations: [
    trigger('fade', [
      transition(':enter', [
        useAnimation(fadeIn)
      ])
    ])
  ]
})
export class WeatherItemComponent implements OnInit, OnDestroy {
  @Input() locationCode!: LocationCode;
  @Output() delete = new EventEmitter<LocationCode>();
  @Output() completed = new EventEmitter();

  confirmDelete!: string;
  conditions: any;

  private alive = true;

  constructor(
    private weatherItemService: WeatherItemService,
    private snackbarService: SnackbarService) {
    this.weatherItemService.weatherConditions$
      .pipe(
        takeWhile(() => this.alive),
      )
      .subscribe(
        (conditions: any) => {
          this.conditions = conditions;
          this.completed.emit();
        },
        (error: any) => {
          this.snackbarService.openSnackBar(error.error.message);
          this.deleteLocationCode(true);
          this.completed.emit();
        });
  }

  ngOnInit(): void {
    this.weatherItemService.locationCode = this.locationCode;
    this.confirmDelete = `Are you sure you want to delete zipcode ${this.locationCode.zipcode}?`;
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  deleteLocationCode(event: boolean): void {
    if (event) {
      this.delete.emit(this.locationCode);
    }
  }
}
