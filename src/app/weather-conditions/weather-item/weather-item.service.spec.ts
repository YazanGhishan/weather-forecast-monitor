import { timer } from 'rxjs';

import { WeatherItemService } from './weather-item.service';

describe('WeatherItemService', () => {
  let service: WeatherItemService;
  let appConfig: any;
  let http: any;
  let timerService: any = { weatherTimer$: timer(0, 1000) };

  beforeEach(() => {
    service = new WeatherItemService(appConfig, http, timerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
