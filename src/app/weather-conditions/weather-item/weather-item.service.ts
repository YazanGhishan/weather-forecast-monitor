import { Inject, Injectable, OnDestroy } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

import { Subject } from 'rxjs';
import { takeWhile } from 'rxjs/operators';

import { APP_CONFIG } from 'src/app/app.module';
import { AppConfig } from 'src/app/app.config.model';
import { LocationCode } from 'src/app/shared/location-code';
import { TimerService } from 'src/app/core/timer.service';

@Injectable()
export class WeatherItemService implements OnDestroy {
  private _locationCode?: LocationCode;

  private weatherConditionsSubject = new Subject<any>();
  weatherConditions$ = this.weatherConditionsSubject.asObservable();

  private alive = true;

  set locationCode(locationCode: LocationCode) { this._locationCode = locationCode; }

  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient,
    private timerService: TimerService) {
    this.timerService.weatherTimer$
      .pipe(takeWhile(() => this.alive))
      .subscribe(() => this.getWeatherConditions());
  }

  ngOnDestroy(): void {
    this.alive = false;
  }

  private getWeatherConditions(): void {
    if (this._locationCode) {
      const zip = this._locationCode.zipcode +
        (this._locationCode.countryCode ? `,${this._locationCode.countryCode}` : '');

      const params = new HttpParams()
        .set('zip', zip)
        .set('units', 'imperial');

      this.http.get<any>(this.config.weatherConditionsUrl, { params })
        .subscribe(
          (conditions: any) => this.weatherConditionsSubject.next(conditions),
          (error: any) => this.weatherConditionsSubject.error(error)
        );
    }
  }
}
