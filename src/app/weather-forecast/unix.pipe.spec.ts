import { UnixPipe } from './unix.pipe';

describe('UnixPipe', () => {
  it('should create an instance', () => {
    const pipe = new UnixPipe();
    expect(pipe).toBeTruthy();
  });

  it('should transform unix to date', () => {
    const pipe = new UnixPipe();
    const unix = 1622826000;
    const expected = 'Fri Jun 04 2021 13:00:00 GMT-0400 (Eastern Daylight Time)';
    const result = pipe.transform(unix).toString();

    expect(result).toBe(expected);
  });
});
