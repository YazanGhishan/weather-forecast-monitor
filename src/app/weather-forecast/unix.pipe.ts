import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'unix'
})
export class UnixPipe implements PipeTransform {
  transform(unix: number): Date {
    return new Date(unix * 1000);
  }
}
