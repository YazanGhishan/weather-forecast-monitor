import { Component, OnInit } from '@angular/core';
import { trigger, transition, useAnimation } from '@angular/animations';
import { ActivatedRoute } from '@angular/router';

import { WeatherService } from '../core/weather.service';
import { fadeIn } from 'src/app/shared/faders';
import { LocationCode } from '../shared/location-code';

@Component({
  selector: 'wfm-weather-forecast',
  templateUrl: './weather-forecast.component.html',
  styleUrls: ['./weather-forecast.component.scss'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        useAnimation(fadeIn)
      ])
    ])
  ]
})
export class WeatherForecastComponent implements OnInit {
  zipcode!: string | null;
  forecast: any;

  constructor(
    private route: ActivatedRoute,
    private weatherService: WeatherService) { }

  ngOnInit(): void {
    const zipcode = <string>this.route.snapshot.paramMap.get('zipcode');
    const countryCode = this.route.snapshot.paramMap.get('countryCode');

    const locationCode: LocationCode = { zipcode: zipcode, countryCode: countryCode };

    if (locationCode) {
      this.weatherService.getWeatherForecast(locationCode)
        .subscribe((forecast: any) => this.forecast = forecast)
    }
  }
}
