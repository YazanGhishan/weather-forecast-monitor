import { NgModule } from '@angular/core';

import { WeatherForecastComponent } from './weather-forecast.component';
import { WeatherForecastRoutingModule } from './weather-forecast-routing.module';
import { SharedModule } from '../shared/shared.module';
import { UnixPipe } from './unix.pipe';

@NgModule({
  declarations: [
    WeatherForecastComponent,
    UnixPipe
  ],
  imports: [
    WeatherForecastRoutingModule,
    SharedModule
  ]
})
export class WeatherForecastModule { }
